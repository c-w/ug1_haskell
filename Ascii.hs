module Ascii
        (
        Ascii,
        legalAscii,
        badAscii,
        drawLines
        )
    where

import LSystem

-- Any ASCII graphic can be represented as a list of strings. Each element of this list is one line of the ASCII graphic.

type Ascii = [String]



-- ***********
-- Functions to convert ASCII graphics to Turtle-Commands
-- ***********

-- Recursive function to draw a single line of an ASCII image

drawLine :: [Char] -> Command
drawLine l = drawL (reverse l)
    where
    drawL :: [Char] -> Command
    drawL [] = Sit
    drawL (c:cs)
        | c == ' '
        = invStep 9 :-: drawL cs
        | c == '/'
        = upR :-: drawL cs
        | c == '\\'
        = upL :-: drawL cs
        | c == '_'
        = underScore :-: drawL cs
        | c == '|'
        = vert :-: drawL cs
        | c == 'o'
        = smallCircle :-: drawL cs
        | c == '0'
        = bigCircle :-: drawL cs
        | c == '('
        = curveL :-: drawL cs
        | c == ')'
        = curveR :-: drawL cs
        | c == '.'
        = point :-: drawL cs
        | c == 'U'
        = u :-: drawL cs
        | c == '^'
        = downV :-: drawL cs
        | c == '>'
        = leftV :-: drawL cs
        | c == '<'
        = rightV :-: drawL cs
        | c == '-'
        = dash :-: drawL cs
        | c == '\''
        = apostrophe :-: drawL cs
        | otherwise
        = skip :-: drawL cs
        -- This function can easily be enlarged to convert more ASCII keys into equivalent graphics as the need for a more complete set of characters arises.

-- Recursive function to draw all the lines of an ASCII image

drawLines :: Ascii -> Command
drawLines [] = Sit
drawLines (l:ls) = drawLine l :-: newLine :-: drawLines ls
    where
    newLine = Turn (-90) :-: invStep 16 :-: Turn (-90) :-: invStep (n*9) :-: Turn 180
        where
        n = fromIntegral (length l)



-- ***********
-- Conversion helper-functions
-- ***********

-- Base Commands to build picture

invStep :: Float -> Command
invStep n = GrabPen Inkless :-: Go n :-: GrabPen black

skip :: Command
skip = GrabPen red :-: Go 3 :-: invStep 3 :-: Go 3 :-: GrabPen black 

upR :: Command
upR = invStep 9 :-: Turn 240.64 :-: Go 18.36 :-: Turn 180 :-: Go 18.36 :-: Turn (-60.64)

upL :: Command
upL = Turn (-60.64) :-: Go 18.36 :-: Turn 180 :-: Go 18.36 :-: Turn (-119.36) :-: invStep 9

underScore :: Command
underScore = Go 9

vert :: Command
vert = invStep 4.5 :-: Turn (-90) :-: Go 16 :-: Turn 180 :-: Go 16 :-: Turn (-90) :-: invStep 4.5

smallCircle :: Command
smallCircle = toPosition :-: drawCircle 2.5 :-: toStart
    where
    toPosition = invStep 4.5 :-: Turn (-90) :-: invStep 4.5 :-: Turn 90
    toStart = Turn 90 :-: invStep 4.5 :-: Turn (-90) :-: invStep 4.5

bigCircle :: Command
bigCircle = toPosition :-: drawCircle 3.5 :-: toStart
    where
    toPosition = invStep 4.5 :-: Turn (-90) :-: invStep 3.5 :-: Turn 90
    toStart = Turn 90 :-: invStep 3.5 :-: Turn (-90) :-: invStep 4.5

curveL :: Command
curveL = copy 180 (Go step :-: Turn (-1)) :-: Turn (-90) :-: invStep 16 :-: Turn (-90) :-: invStep 9
    where
    step = 2*pi*8/360

curveR :: Command
curveR = invStep 9 :-: Turn 180 :-: copy 180 (Go step :-: Turn 1) :-: Turn 90 :-: invStep 16 :-: Turn (-90)
    where
    step = 2*pi*8/360

point :: Command
point = invStep 4.5 :-: drawCircle 0.5 :-: invStep 4.5

u :: Command
u = invStep 4.5 :-: Turn (-60) :-: copy 30 (Go step :-: Turn (-1)) :-: Turn (-90) :-: invStep 9 :-: Turn (-90) :-: copy 30 (Go step :-: Turn (-1)) :-: Turn (-60) :-: invStep 4.5
    where
    step = 2*pi*32/360

downV :: Command
downV = Turn (-90) :-: invStep 5 :-: Turn 30 :-: invStep 3 :-: Go 7 :-: Turn 120 :-: Go 7 :-: invStep 3 :-: Turn 30 :-: invStep 5 :-: Turn (-90)

leftV :: Command
leftV = Turn (-90) :-: invStep 5 :-: Turn 120 :-: Go 10 :-: Turn (-120) :-: invStep 10 :-: Turn (-120) :-: Go 10 :-: Turn (-60) :-: invStep 5 :-: Turn (-90) :-: invStep 9

rightV :: Command
rightV = Turn (-30) :-: Go 10 :-: Turn (-120) :-: Go 10 :-: Turn (-120) :-: invStep 10 :-: Turn (-90) :-: invStep 9

dash :: Command
dash = Turn (-90) :-: invStep 8 :-: Turn 90 :-: Go 9 :-: Turn 90 :-: invStep 8 :-: Turn (-90)

apostrophe :: Command
apostrophe = invStep 4.5 :-: Turn (-90) :-: invStep 10 :-: Go 6 :-: Turn 180 :-: invStep 16 :-: Turn (-90) :-: invStep 4.5

-- Auxiliary functions to handle Commands

split :: Command -> [Command]
split (Go d) = [Go d]
split (Turn a) = [Turn a]
split (Sit) = []
split (c1 :-: c2) = split c1 ++ split c2

join :: [Command] -> Command
join cs = foldr (:-:) Sit cs

copy :: Int -> Command -> Command
copy x = join . concat . replicate x . split

drawCircle :: Float -> Command
drawCircle r = copy 360 (Go step :-: Turn (-1))
    where
    step = 2*pi*r/360

test :: Command -> IO ()
test c = display (Turn 90 :-: c)

-- ***********
-- Functions to check input to draw functions
-- ***********

-- Function to check whether a list of Strings is a well formed ASCII image

legalAscii :: Ascii -> Bool
legalAscii a = and [length l1 == length l2 | (l1,l2) <- zip a (tail a)]

-- Error-Message for ill-formed ASCIIs

badAscii :: String
badAscii = "Argument is not a well formed ASCII graphic."
