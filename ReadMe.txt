This entry won third place in the 2010 rendition of the "Galois Haskell
Competition", a first year competition run by the University of Edinburgh.
Objective: Do something interesting with pictures.
Prescribed language: Haskell.
Duration: 2 weeks.
