import Ascii -- Import auxiliary ASCII-handling functions
import LSystem -- Import Turtle-Graphics environment

-- ***********
-- Functions to display ASCII graphics
-- ***********

-- Function to print an ASCII-image as a Turtle Graphic

asciiToTurtle :: Ascii -> IO ()
asciiToTurtle a
    | legalAscii a
    = display (Turn 90 :-: drawLines (reverse a))
    | otherwise
    = error badAscii

-- Function to show an ASCII-image by printing it to the Terminal

asciiToTerminal :: Ascii -> IO ()
asciiToTerminal a
    | legalAscii a
    = putStr (unlines a)
    | otherwise
    = error badAscii



-- ***********
-- Some sample ASCII graphics for testing
-- ***********

-- More complex ASCII graphics

hedgehog :: Ascii
hedgehog =  [   "   |||||||||    ",
                " |||||||||||||  ",
                "|||||||||||  o\\ ",
                " ||||||||||____o"
            ]

dinosaur :: Ascii
dinosaur =  [   "                __ ",
                "               /  )",
                "      _/\\/\\/\\_/ /  ",
                "    _|         /   ",
                "  _|  (  |  ( |    ",
                " /__.-'|_|--|_|    "
            ]

smallbird :: Ascii
smallbird = [   ">o)",
                "(_>"
            ]

fox :: Ascii
fox =   [   "   |\\/|     ____",
            "o__.. \\    /\\  /",
            " \\_   /___/  \\/ ",
            " _/   ___   _/  ",
            "/____/ ____/    "
        ]
            
owl :: Ascii
owl =   [   "___/|",
            "\\o.0|",
            "(___)",
            "  U  "
        ]
            
cat :: Ascii
cat =   [   " /\\_/\\ ",
            "( o.o )",
            " > ^ < "
        ]

dog :: Ascii
dog =   [   "^..^      /",
            "/_/\\_____/ ",
            "   /\\   /\\ ",
            "  /  \\ /  \\"
        ]

chicken :: Ascii
chicken =   [   "   \\\\ ",
                "   (o>",
                "\\\\_//)",
                " \\_/_)",
                "   |  ",
                "  _|_ "
            ]
        
-- Very basic ASCII graphics for preliminary testing

badone :: Ascii
badone =    [   ".....",
                ".....",
                ".....",
                "..."
            ]
            
lamda :: Ascii
lamda = [   "  \\    ",
            "   \\   ",
            "   /\\  ",
            "  /  \\ "
        ]
